# Configuration
You'll first need to get your SOCKET TOKEN from streamlabs on the [API Settings](https://streamlabs.com/dashboard#/apisettings) section in the *API TOKENS* tab, there you will see *Your Socket API Token*, copy that and paste it in the *js/juju-js* file as well as the *min_donation* variable to set the minumum amount that will trigger the shot alert as shown below:

<pre>
const socketToken = 'SOCKET_TOKEN';
min_donation = 10;
</pre>