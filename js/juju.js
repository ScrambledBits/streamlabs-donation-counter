$('document').ready(function () {
    const socketToken =
        'SOCKET_TOKEN';
    const streamlabs = io(`https://sockets.streamlabs.com?token=${socketToken}`);

    var shot_counter = 0;
    var min_donation = 0;

    streamlabs.on('connect', (eventData) => {
        console.log('We got connected!!!');
    })

    //Perform Action on event
    streamlabs.on('event', (eventData) => {
        if (eventData.type === 'donation' && eventData.message[0].amount >= min_donation) {
            // code to handle donation events
            shot_counter++;
            $('#output').append('<div class="col-2"><img src="img/shot.gif" class="img-fluid shot-glass" /></div>');
            $('#shot_counter').text(shot_counter);
        }
    });

    $('#output').on('click', '.shot-glass', function () {
        $(this).closest('.col-2').remove();
        shot_counter--;
        $('#shot_counter').text(shot_counter);
    });
});